cmake_minimum_required(VERSION 3.5)

project(qqwtest VERSION 0.1 LANGUAGES CXX)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt6 REQUIRED COMPONENTS Widgets QuickWidgets)

set(PROJECT_SOURCES
        main.cpp
        mainwindow.cpp
        mainwindow.h
)

add_executable(qqwtest
    ${PROJECT_SOURCES}
    qml.qrc
)

target_link_libraries(qqwtest PRIVATE Qt6::Widgets Qt6::QuickWidgets)
