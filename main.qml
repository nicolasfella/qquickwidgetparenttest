import QtQuick 2.15

Rectangle {
    id: root

    color: "red"

    width: 500
    height: 500

    property Window w: Window.window

    Window {
        visible:  true
        transientParent: root.w
    }
}
