#include "mainwindow.h"

#include <QQuickWidget>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    auto w = new QQuickWidget;

    w->setSource(QUrl("qrc:///main.qml"));

    setCentralWidget(w);
}

MainWindow::~MainWindow()
{
}

